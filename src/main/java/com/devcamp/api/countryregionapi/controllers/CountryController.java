package com.devcamp.api.countryregionapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.api.countryregionapi.models.Country;
import com.devcamp.api.countryregionapi.services.CountryService;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


@CrossOrigin
@RestController
public class CountryController {
    @Autowired
    CountryService countryService;

    @GetMapping(value="/countries")
    public ArrayList<Country> getAllCountries() {
        //CountryService countryService = new CountryService();
        ArrayList<Country> countries = countryService.getAllCountries();
        
        return countries;
    }
    
    @GetMapping(value="/country-info")
    public Country getMethodName(@RequestParam(value="code", defaultValue = "vn") String countryCode) {
        return countryService.getCountryByCountryCode(countryCode);
    }    
}
